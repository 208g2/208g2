package com.higgsup.fswd.classroommanager.controller;

import com.higgsup.fswd.classroommanager.controller.stereotype.NoAuthentication;
import com.higgsup.fswd.classroommanager.controller.stereotype.RequiredRoles;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by lent on 4/20/2016.
 */
@RestController
public class TestRoleController {
    @NoAuthentication
    @RequestMapping("/isGuest")
    public String isGuest() {
        return "Yes, guest is accessible";
    }

    @RequiredRoles("Registered")
    @RequestMapping("/isRegistered")
    public String isRegistered() {
        return "Yes, registered user is accessible";
    }

    @RequiredRoles("Administrator")
    @RequestMapping("/isAdministrator")
    public String isAdministrator() {
        return "Yes, administrator is accessible";
    }

    @RequiredRoles({"Administrator", "Registered"})
    @RequestMapping("/isNotGuest")
    public String isNotGuest() {
        return "Yes, user in the system is accessible";
    }
}
