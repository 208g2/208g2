package com.higgsup.fswd.classroommanager.repository;

import com.higgsup.fswd.classroommanager.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<Student, String> {
}
